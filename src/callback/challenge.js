let XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
const API = 'https://rickandmortyapi.com/api/character';

function fetchData(urlApi, cb) {
  let xhttp = new XMLHttpRequest();
  xhttp.open('GET', urlApi, true);
  xhttp.onreadystatechange = function(event) {
    if(xhttp.readyState === 4) {
      if(xhttp.status === 200) {
        cb(null, JSON.parse(xhttp.responseText));
      } else {
        const error = new Error('Error ', urlApi);
        return cb(error, null);
      }
    }
  };
  xhttp.send();
}

fetchData(API, function(err, data) {
  if(err) {
    return console.error(err);
  }
  fetchData(API + data.results[0].id, function(err1, data1) {
    if(err1){
      return console.error(err1);
    }
    fetchData(data.origin.url, function(err2, data2) {
      if(err2){
        return console.error(err2);
      }
      console.log(data.info.count);
      console.log(data1.name);
      console.log(data2.dimension);
    });
  });
});