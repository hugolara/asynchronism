const somethingWillHappen = () => {
  return new Promise((resolve, reject) => {
    if(true) {
      resolve('we did it!');
    } else {
      reject('Oops!');
    }
  });
};

somethingWillHappen()
  .then((response) => {
    console.log(response);
  })
  .catch((err) => console.error(err));

const somethingWillHappen2 = (() => {
  return new Promise((resolve, reject) => {
    if(true) {
      setTimeout(() => {
        resolve('Resuelto timeOut');
      }, 5000);
    } else {
      const err = new Error('No ves que valio verga?');
      reject(err);
    }
  });
});

somethingWillHappen2()
  .then((response) => {
    console.log(response);
  })
  .catch((err) => {
    console.error(err);
  });

Promise.all([somethingWillHappen(), somethingWillHappen2()])
  .then( response => {
    console.log('Arraay of results: ', response);
  })
  .catch(err => {
    console.error(err);
  });