let XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
const https = require('https')

// const fetchDat2 = (urlApi) => {
//   return new Promise((resolve, reject) => {
//     const xhttp = new XMLHttpRequest();
//     xhttp.open('GET', urlApi, true);
//     xhttp.onreadystatechange = (() => {
//       if(xhttp.readyState === 4) {
//         (xhttp.status === 200)
//           ? resolve(JSON.parse(xhttp.responseText)) : reject(new Error('Error: ', urlApi));
//       }
//     });
//     xhttp.send();
//   });
// };

const fetchData = (urlApi) => {
  return new Promise((resolve, reject) => {
    https.get(urlApi, (res) => {
      res.setEncoding('utf-8');
      if(res.statusCode === 200) {
        let body = '';
        res.on('data', (data) => {
          body += data;
        });
        res.on('end', () => {
          resolve(JSON.parse(body));
        });
      } else {
        reject(new Error(`Request errpr on ${urlApi}.Status ${res.statusCode}`));
      }
    });
  });
};

module.exports = fetchData;